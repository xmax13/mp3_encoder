#include "encodingtask.h"

EncodingTask::EncodingTask(QObject *parent) : QObject(parent)
{
    try {
        QCoreApplication *app = dynamic_cast<QCoreApplication*>(parent);
        int argc = app->arguments().count();
        in_path = "";
        if (argc > 1)
            in_path = app->arguments().takeAt(1);
    }
    catch (...) {
        in_path = "";
    }
}

void encode_file(QString file_name)
{

    FileEncoder *encoder = new FileEncoder(file_name);
    encoder->encode();
    delete encoder;
}

void EncodingTask::encode()
{
    // Check for an input path
    if (in_path == "") {
        qCritical() << "Input files folder is not found. Specify the input folder.\n";
        return;
    }
    // Check if the input folder exists
    QDir *dir = new QDir(in_path);
    if (!dir->exists()) {
        delete dir;
        qCritical() << "Input files folder does not exist. Specify an existsing folder.\n";
        return;
    }
    qInfo() << "Convert files in:" << in_path;

    // Look for all files in the folder
    QStringList filter("*.wav");
    QStringList files = dir->entryList(filter);
    delete dir;
    qInfo() << "Total file count:" << files.count();
    std::vector<std::thread> thread_pool;
    for (QStringList::iterator f = files.begin();
         f != files.end(); ++f) {
        QString fn = *f;
        QString full_name = QDir(in_path).filePath(fn);
        thread_pool.push_back(std::thread(encode_file, full_name));
    }
    for(auto &item : thread_pool)
    {
        item.join();
    }
}
