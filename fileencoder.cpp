#include "fileencoder.h"

FileEncoder::FileEncoder(QString file_name)
{
    fileName = file_name;
    QFileInfo info(fileName);
    mp3FileName = info.path() + "/" + info.completeBaseName() + ".mp3";
}

void FileEncoder::encode() {

    qInfo() << "Converting" << fileName;
    if (validate_file()) {
        QFile mp3File(mp3FileName);
        if (!mp3File.open(QIODevice::WriteOnly)) {
            qCritical() << "\tUnable to create file" << fileName;
            return;
        }
        QFile wavFile(fileName);
        if (!wavFile.open(QIODevice::ReadOnly)) {
            qCritical() << "\tUnable to open file" << fileName;
            return;
        }

        WAVHEADER header;
        uint readbytes = wavFile.read(reinterpret_cast<char *>(&header), sizeof(header));
        if (readbytes == sizeof(header)) {

            int PCM_SIZE = 4096;
            int MP3_SIZE = 8192;
            short int pcm_buffer[PCM_SIZE];
            unsigned char mp3_buffer[MP3_SIZE];

            lame_t lame = lame_init();
            if ( lame != NULL ) {
                lame_set_num_channels(lame, header.numChannels);
                lame_set_in_samplerate(lame, header.sampleRate);
                lame_set_out_samplerate(lame, header.sampleRate);
                lame_set_brate(lame, 128);
                lame_set_quality(lame, 5);
                lame_set_bWriteVbrTag(lame, 0);
                if ( lame_init_params(lame) >= 0) {
                    uint bytesToWrite;
                    do
                    {
                        readbytes = wavFile.read((char*)pcm_buffer, PCM_SIZE * 2);
                        if (readbytes == 0)
                            bytesToWrite = lame_encode_flush(
                                        lame, mp3_buffer, MP3_SIZE);
                        else {
                            int samples = (readbytes / 2) / 2;
                            bytesToWrite = lame_encode_buffer_interleaved(
                                        lame, pcm_buffer, samples,
                                        mp3_buffer, MP3_SIZE);
                        }
                        mp3File.write((char*)mp3_buffer, bytesToWrite);
                    } while (readbytes != 0);
                }
                else
                    qInfo() << "\tUnable to initialize encoding parameters:" << fileName;
                lame_close(lame);
            }
            else
                qInfo() << "\tUnable to initialize encoder:" << fileName;
        }
        wavFile.close();
        mp3File.close();
        qInfo() << mp3FileName << "Converted";
    }
}

bool FileEncoder::validate_file() {

    bool res = false;
    QFile file(fileName);
    // Check if the file exists
    if (!file.open(QIODevice::ReadOnly)) {
        qCritical() << "\t" << fileName << "does not exist";
        return false;
    }
    // Check the WAV file's header.
    // It has to match 5 conditions (header size, RIFF, WAVE, fmt, audio-format)
    WAVHEADER header;
    int readbytes = file.read(reinterpret_cast<char *>(&header), sizeof(header));
    if (readbytes == sizeof(header)) {
        int cc = 0;
        if (strncmp(header.chunkId, "RIFF", 4) == 0)
            cc++;
        if (strncmp(header.format, "WAVE", 4) == 0)
            cc++;
        if (strncmp(header.subchunk1Id, "fmt ", 4) == 0)
            cc++;
        if (header.audioFormat == 1)
            cc++;
        if (cc == 4)
            res = true;
    }
    file.close();
    if (!res)
        qCritical() << "\t" << fileName << "is not valid WAV file";

    return res;
}
