#include <QCoreApplication>
#include <QtCore>
#include "encodingtask.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    EncodingTask *task = new EncodingTask(&a);
    task->encode();
    delete task;

    return 0;
}
