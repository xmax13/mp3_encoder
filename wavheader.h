#ifndef WAVHEADER_H
#define WAVHEADER_H

// WAV Header
struct WAVHEADER
{
    // RIFF-title

    // ASCII Symbols "RIFF" - (0x52494646 in big-endian)
    char chunkId[4];

    // 4 + (8 + subchunk1Size) + (8 + subchunk2Size)
    // Rest size of a chain, starting with this position
    unsigned long chunkSize;

    // Symbols "WAVE" (0x57415645 in big-endian)
    char format[4];

    // "WAVE" contains 2 sub-chains: "fmt " and "data":
    // Sub-chain "fmt " describes the sound data format

    // Symbols "fmt " (0x666d7420 in big-endian)
    char subchunk1Id[4];

    // 16 for PCM
    unsigned long subchunk1Size;

    // Audio-format
    // 1 for PCM
    unsigned short audioFormat;

    // Number of channels. Mono = 1, Stereo = 2 etc
    unsigned short numChannels;

    // Samplerate
    unsigned long sampleRate;

    // sampleRate * numChannels * bitsPerSample/8
    unsigned long byteRate;

    // numChannels * bitsPerSample/8
    unsigned short blockAlign;

    // Bits per sample
    unsigned short bitsPerSample;

    // Sub-chain "data" contains audio-data and its size

    // Symbols "data" (0x64617461 in big-endian)
    char subchunk2Id[4];

    // numSamples * numChannels * bitsPerSample/8
    // Number of bytes in the data area
    unsigned long subchunk2Size;
};

#endif // WAVHEADER_H
