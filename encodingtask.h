#ifndef ENCODINGTASK_H
#define ENCODINGTASK_H

#include <QDebug>
#include <QCoreApplication>
#include <QObject>
#include <QDir>
#include <thread>
#include "fileencoder.h"

class EncodingTask : public QObject
{
    Q_OBJECT
private:
    QString in_path = "";
public:
    explicit EncodingTask(QObject *parent = 0);
public slots:
    void encode();
};

#endif // ENCODINGTASK_H
