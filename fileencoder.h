#ifndef FILEENCODER_H
#define FILEENCODER_H

#include <QtCore>
#include "lame.h"
#include "wavheader.h"

class FileEncoder
{
private:
    QString fileName = "";
    QString mp3FileName = "";

    bool validate_file();
public:
    FileEncoder(QString file_name);
    void encode();
};

#endif // FILEENCODER_H
